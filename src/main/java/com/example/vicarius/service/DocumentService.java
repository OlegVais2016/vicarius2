package com.example.vicarius.service;

import com.example.vicarius.model.entity.DocumentIndex;
import com.example.vicarius.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    public DocumentIndex saveDocument(DocumentIndex documentIndex) {
        return documentRepository.save(documentIndex);
    }
    public DocumentIndex getById( Integer id) {
        DocumentIndex documentIndex  = documentRepository.findById(id).get();

        return documentIndex;
    }

}
