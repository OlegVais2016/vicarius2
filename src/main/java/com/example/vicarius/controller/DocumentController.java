package com.example.vicarius.controller;

import com.example.vicarius.model.entity.DocumentIndex;
import com.example.vicarius.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class DocumentController {
    @Autowired
    private DocumentService documentService;
    @PostMapping("/insert")
    public DocumentIndex saveDocument(@RequestBody DocumentIndex documentIndex){
        return documentService.saveDocument(documentIndex);
    }
    @GetMapping("/{id}")
    public DocumentIndex getById(@PathVariable("id") Integer id) {
        return documentService.getById(id);
    }


}
