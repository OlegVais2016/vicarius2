package com.example.vicarius.repository;

import com.example.vicarius.model.entity.DocumentIndex;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface DocumentRepository extends ElasticsearchRepository<DocumentIndex,Integer> {

}
