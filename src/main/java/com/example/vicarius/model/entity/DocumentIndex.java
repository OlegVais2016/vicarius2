package com.example.vicarius.model.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(indexName = "userindex1")
public class DocumentIndex {
    private Integer id;
    private String name;
    private String description;
}
